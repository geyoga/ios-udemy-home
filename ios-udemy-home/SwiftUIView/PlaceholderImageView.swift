//
//  PlaceholderImageView.swift
//  ios-udemy-home
//
//  Created by Planet iOS on 24/06/24.
//

import SwiftUI

struct PlaceholderImageView: View {
    var body: some View {
        Rectangle()
            .foregroundColor(Color.gray.opacity(0.3))
    }
}

#Preview {
    PlaceholderImageView()
}
