//
//  CourseView.swift
//  ios-udemy-home
//
//  Created by Planet iOS on 24/06/24.
//

import SwiftUI

struct CourseView: View {
    let imageLink: String
    let title: String
    let author: String
    let rating: Double
    let reviewCount: Int
    let price: Decimal
    let tag: String
    var onTap:(() -> Void)?
    
    var body: some View {
        VStack(alignment: .leading, spacing: 4) {
            AsyncImage(url: URL(string: imageLink)) { image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(maxWidth: .infinity)
                    .frame(height: 64)
                    .border(Color.gray.opacity(0.3))
                    .clipped()
            } placeholder: {
                PlaceholderImageView()
                    .frame(height: 64)
            }.padding(.bottom, 4)
            Text(title)
                .font(.system(size: 12, weight: .bold, design: .default))
                .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                .lineLimit(3)
            Text(author)
                .font(.system(size: 10, weight: .regular, design: .default))
                .foregroundStyle(.gray)
            ReviewRatingView(rating: rating, reviewCount: reviewCount)
            Text(price.priceFormat)
                .font(.system(size: 10, weight: .bold))
            Text(tag)
                .font(.system(size: 10, weight: .semibold))
                .padding(EdgeInsets(top: 4, leading: 8, bottom: 4, trailing: 8))
                .background(content: {
                    RoundedRectangle(cornerRadius: 2)
                        .fill(Color.yellow.opacity(0.4))
                })
            Spacer()
        }
        .onTapGesture {
            onTap?()
        }
    }
}

#Preview {
    CourseView(
        imageLink: "https://images.unsplash.com/photo-1587620962725-abab7fe55159?q=80&w=1931&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
        title: "Laptop with code and plant in coffee shop",
        author: "James Harrison",
        rating: 5.0,
        reviewCount: 34,
        price: 2999,
        tag: "Recommend"
    )
}
