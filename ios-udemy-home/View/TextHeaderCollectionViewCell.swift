//
//  TextHeaderCollectionViewCell.swift
//  ios-udemy-home
//
//  Created by Planet iOS on 24/06/24.
//

import UIKit

final class TextHeaderCollectionViewCell: UICollectionViewCell {

    private let label: AttributedTappableLabel = AttributedTappableLabel()

    var onTap: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(text: String, higlightedText: String?) {
        label.setAttributedText(
            text: text,
            highlightedText: higlightedText,
            color: .systemIndigo,
            font: .systemFont(ofSize: 18, weight: .bold)
        )
    }

    private func layout() {
        addSubview(label)
        label.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        label.onTap = { [weak self] in
            guard let self = self else { return }
            self.onTap?()
        }
    }
}
