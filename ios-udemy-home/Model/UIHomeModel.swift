//
//  UIHomeModel.swift
//  ios-udemy-home
//
//  Created by Planet iOS on 24/06/24.
//

import Foundation

struct HomeUIModel: Hashable {

    let sectionModels: [SectionModel]

    struct SectionModel: Hashable {
        let section: Section
        let body: [Item]
    }
    
    enum Section: Hashable {
        case mainBanner(id: String)
        case textHeader(id: String)
        case coursesSwimlane(id: String)
        case udemyBusinessBanner(id: String)
        case categories(id: String)
        case featureCourse(id: String)
    }

    enum Item: Hashable {
        case mainBanner(id: String, imageLink: String, title: String, caption: String)
        case course(id: String, imageLink: String, title: String, author: String, rating: Double, reviewCount: Int, price: Decimal, tag: String)
        case textHeader(id: String, text: String, higlighterText: String?)
        case udemyBusinessBanner(id: String, link: String)
        case categoriesScroller(id: String, titles: [String])
        case featureCourse(id: String, imageLink: String, title: String, author: String, rating: Double, reviewCount: Int, price: Decimal)
    }
}
