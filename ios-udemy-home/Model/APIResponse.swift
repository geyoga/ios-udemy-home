//
//  APIResponse.swift
//  ios-udemy-home
//
//  Created by Planet iOS on 24/06/24.
//

import Foundation

struct APIResponse: Decodable {
    let status: Int
    let layouts: [Layout]

    enum Layout: Decodable, Hashable {
        
        case mainBanner(String, MainBanner)
        case textHeader(String, TextHeader)
        case courseSwimlane(String, [Course])
        case categories(String, Categories)
        case featureCourse(String, Course)
        case udemyBusinessBanner(String, UdemyBusinessBanner)

        enum CodingKeys: CodingKey {
            case mainBanner
            case textHeader
            case courseSwimlane
            case categories
            case featureCourse
            case udemyBusinessBanner
        }
        
        enum MainBannerCodingKeys: CodingKey {
            case _0
            case _1
        }
        
        enum TextHeaderCodingKeys: CodingKey {
            case _0
            case _1
        }
        
        enum CourseSwimlaneCodingKeys: CodingKey {
            case _0
            case _1
        }
        
        enum CategoriesCodingKeys: CodingKey {
            case _0
            case _1
        }
        
        enum FeatureCourseCodingKeys: CodingKey {
            case _0
            case _1
        }
        
        enum UdemyBusinessBannerCodingKeys: CodingKey {
            case _0
            case _1
        }
        
        init(from decoder: any Decoder) throws {
            let container = try decoder.container(keyedBy: APIResponse.Layout.CodingKeys.self)
            var allKeys = ArraySlice(container.allKeys)
            guard let onlyKey = allKeys.popFirst(), allKeys.isEmpty else {
                throw DecodingError.typeMismatch(APIResponse.Layout.self, DecodingError.Context.init(codingPath: container.codingPath, debugDescription: "Invalid number of keys found, expected one.", underlyingError: nil))
            }
            switch onlyKey {
            case .mainBanner:
                let nestedContainer = try container.nestedContainer(keyedBy: APIResponse.Layout.MainBannerCodingKeys.self, forKey: APIResponse.Layout.CodingKeys.mainBanner)
                self = APIResponse.Layout.mainBanner(try nestedContainer.decode(String.self, forKey: APIResponse.Layout.MainBannerCodingKeys._0), try nestedContainer.decode(APIResponse.MainBanner.self, forKey: APIResponse.Layout.MainBannerCodingKeys._1))
            case .textHeader:
                let nestedContainer = try container.nestedContainer(keyedBy: APIResponse.Layout.TextHeaderCodingKeys.self, forKey: APIResponse.Layout.CodingKeys.textHeader)
                self = APIResponse.Layout.textHeader(try nestedContainer.decode(String.self, forKey: APIResponse.Layout.TextHeaderCodingKeys._0), try nestedContainer.decode(APIResponse.TextHeader.self, forKey: APIResponse.Layout.TextHeaderCodingKeys._1))
            case .courseSwimlane:
                let nestedContainer = try container.nestedContainer(keyedBy: APIResponse.Layout.CourseSwimlaneCodingKeys.self, forKey: APIResponse.Layout.CodingKeys.courseSwimlane)
                self = APIResponse.Layout.courseSwimlane(try nestedContainer.decode(String.self, forKey: APIResponse.Layout.CourseSwimlaneCodingKeys._0), try nestedContainer.decode([APIResponse.Course].self, forKey: APIResponse.Layout.CourseSwimlaneCodingKeys._1))
            case .categories:
                let nestedContainer = try container.nestedContainer(keyedBy: APIResponse.Layout.CategoriesCodingKeys.self, forKey: APIResponse.Layout.CodingKeys.categories)
                self = APIResponse.Layout.categories(try nestedContainer.decode(String.self, forKey: APIResponse.Layout.CategoriesCodingKeys._0), try nestedContainer.decode(APIResponse.Categories.self, forKey: APIResponse.Layout.CategoriesCodingKeys._1))
            case .featureCourse:
                let nestedContainer = try container.nestedContainer(keyedBy: APIResponse.Layout.FeatureCourseCodingKeys.self, forKey: APIResponse.Layout.CodingKeys.featureCourse)
                self = APIResponse.Layout.featureCourse(try nestedContainer.decode(String.self, forKey: APIResponse.Layout.FeatureCourseCodingKeys._0), try nestedContainer.decode(APIResponse.Course.self, forKey: APIResponse.Layout.FeatureCourseCodingKeys._1))
            case .udemyBusinessBanner:
                let nestedContainer = try container.nestedContainer(keyedBy: APIResponse.Layout.UdemyBusinessBannerCodingKeys.self, forKey: APIResponse.Layout.CodingKeys.udemyBusinessBanner)
                self = APIResponse.Layout.udemyBusinessBanner(try nestedContainer.decode(String.self, forKey: APIResponse.Layout.UdemyBusinessBannerCodingKeys._0), try nestedContainer.decode(APIResponse.UdemyBusinessBanner.self, forKey: APIResponse.Layout.UdemyBusinessBannerCodingKeys._1))
            }
        }
    }

    struct MainBanner: Decodable, Hashable {
        let id: String
        let imageLink: String
        let title: String
        let caption: String
    }
    struct TextHeader: Decodable, Hashable {
        let id: String
        let text: String
        let highlightedText: String?
    }
    struct Course: Decodable, Hashable {
        let id: String
        let imageLink: String
        let title: String
        let author: String
        let rating: Double
        let reviewCount: Int
        let price: Decimal
        let tag: String
    }
    struct Categories: Decodable, Hashable {
        let id: String
        let titles: [String]
    }
    struct UdemyBusinessBanner: Decodable, Hashable {
        let id: String
        let link: String
    }
}


