//
//  Category.swift
//  ios-udemy-home
//
//  Created by Planet iOS on 24/06/24.
//

import Foundation

enum Category: String, CaseIterable {
    case development
    case business
    case officeProductivity
    case healthAndFitness
    case teachingAndAccounting
    case financeAccounting
    case itAndSoftware
    case personalDevelopment
    case marketing
    case photographyAndVideo
    case design
    case lifestyle
    case music
}
