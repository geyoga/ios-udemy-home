//
//  Extension+UICollectionViewCell.swift
//  ios-udemy-home
//
//  Created by Planet iOS on 24/06/24.
//

import UIKit

extension UICollectionViewCell {
    static var nameIdentifier: String {
        return String(describing: self)
    }
}
