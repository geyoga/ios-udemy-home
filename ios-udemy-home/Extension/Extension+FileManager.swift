//
//  Extension+FileManager.swift
//  ios-udemy-home
//
//  Created by Planet iOS on 27/06/24.
//

import Foundation

extension FileManager {
    static func modelFromJSON<T: Decodable>(fileName: String) -> T? {
        guard let path = Bundle.main.path(forResource: fileName, ofType: "json") else { 
            print("path not found")
            return nil
        }
        do {
            let data  = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            return try JSONDecoder().decode(T.self, from: data)
        } catch {
            print("Error reading JSON file \(error)")
            return nil
        }
    }
}
