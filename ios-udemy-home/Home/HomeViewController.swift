//
//  HomeViewController.swift
//  ios-udemy-home
//
//  Created by Planet iOS on 21/06/24.
//

import UIKit
import SnapKit
import Combine
import SafariServices

class HomeViewController: UIViewController {

    private let collectionView = HomeCollectionView()
    private var cancellabless = Set<AnyCancellable>()

    override func loadView() {
        super.loadView()

        observe()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadJSON()
        setupView()

        let uiModel = HomeUIModel(sectionModels: [
            .init(section: .mainBanner(id: "123"), body: [
                .mainBanner(
                    id: "123123",
                    imageLink: "https://images.unsplash.com/photo-1512941937669-90a1b58e7e9c?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    title: "Learning that fits",
                    caption: "Skills for your future and present"
                )
            ]),
            .init(section: .textHeader(id: "12323"), body: [
                .textHeader(
                    id: "4342",
                    text: "Newest Courses in Mobile Development",
                    higlighterText: "Mobile Development"
                )
            ]),
            .init(section: .coursesSwimlane(id: "3434"), body: [
                .course(
                    id:"01",
                    imageLink: "https://www.udemy.com/staticx/udemy/js/webpack/coding-exercises-demo-preview-desktop.2957bed27c3ae43a02824b61ad9cda03.png",
                    title: "iOS & Swift - The Complete iOS App Development Bootcamp",
                    author: "James Harrison",
                    rating: 4.7,
                    reviewCount: 382387,
                    price: 119000,
                    tag: "Bestseller"
                ),
                .course(
                    id:"02",
                    imageLink: "https://images.unsplash.com/photo-1559651451-4203db8d2776?q=80&w=2012&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    title: "The Ultimate 70+ Hours iOS Development Bootcamp",
                    author: "James Harrison",
                    rating: 5.0,
                    reviewCount: 34,
                    price: 2999,
                    tag: "Recommend"
                ),
                .course(
                    id:"03",
                    imageLink: "https://images.unsplash.com/photo-1526498460520-4c246339dccb?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    title: "Beginner to Senior Developer! SwiftUI, MV Pattern, Core Data, SwiftData, Vapor, MapKit, Testing, Combine and much more!",
                    author: "James Harrison",
                    rating: 5.0,
                    reviewCount: 34,
                    price: 2999,
                    tag: "Newbie Friendly"
                ),
                .course(
                    id:"04",
                    imageLink: "https://plus.unsplash.com/premium_photo-1661373704604-7c4d230c8928?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8bW9iaWxlJTIwZGV2ZWxvcG1lbnR8ZW58MHx8MHx8fDA%3D",
                    title: "Understand and implement the Model-View design pattern to structure iOS apps",
                    author: "James Harrison",
                    rating: 5.0,
                    reviewCount: 34,
                    price: 2999,
                    tag: "Recommend"
                )
            ]),
            .init(section: .textHeader(id: "3433"), body: [
                .textHeader(
                    id: "1211",
                    text: "Categories",
                    higlighterText: nil
                )
            ]),
            .init(section: .categories(id: "76767"), body: [
                .categoriesScroller(id: "45454", titles: Category.allCases.map({
                    $0.rawValue.camelCaseToEnglish.useShortAndFormat.capitalized
                }))
            ]),
            .init(section: .textHeader(id: "9989"), body: [
                .textHeader(
                    id: "65",
                    text: "Because you viewed \"How to land yourself a role\"",
                    higlighterText: "How to land yourself a role"
                )
            ]),
            .init(section: .coursesSwimlane(id: "34"), body: [
                .course(
                    id:"565656",
                    imageLink: "https://www.udemy.com/staticx/udemy/js/webpack/coding-exercises-demo-preview-desktop.2957bed27c3ae43a02824b61ad9cda03.png",
                    title: "iOS & Swift - The Complete iOS App Development Bootcamp",
                    author: "James Harrison",
                    rating: 4.7,
                    reviewCount: 382387,
                    price: 119000,
                    tag: "Bestseller"
                ),
                .course(
                    id:"454545",
                    imageLink: "https://images.unsplash.com/photo-1559651451-4203db8d2776?q=80&w=2012&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    title: "The Ultimate 70+ Hours iOS Development Bootcamp",
                    author: "James Harrison",
                    rating: 5.0,
                    reviewCount: 34,
                    price: 2999,
                    tag: "Recommend"
                ),
                .course(
                    id:"454",
                    imageLink: "https://images.unsplash.com/photo-1526498460520-4c246339dccb?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    title: "Beginner to Senior Developer! SwiftUI, MV Pattern, Core Data, SwiftData, Vapor, MapKit, Testing, Combine and much more!",
                    author: "James Harrison",
                    rating: 5.0,
                    reviewCount: 34,
                    price: 2999,
                    tag: "Newbie Friendly"
                ),
                .course(
                    id:"4545",
                    imageLink: "https://plus.unsplash.com/premium_photo-1661373704604-7c4d230c8928?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8bW9iaWxlJTIwZGV2ZWxvcG1lbnR8ZW58MHx8MHx8fDA%3D",
                    title: "Understand and implement the Model-View design pattern to structure iOS apps",
                    author: "James Harrison",
                    rating: 5.0,
                    reviewCount: 34,
                    price: 2999,
                    tag: "Recommend"
                )
            ]),
            .init(section: .textHeader(id: "567"), body: [
                .textHeader(
                    id: "766",
                    text: "Top Course of the year",
                    higlighterText: nil
                )
            ]),
            .init(section: .featureCourse(id: "45475676"), body: [
                .featureCourse(
                    id:"6796556",
                    imageLink: "https://images.unsplash.com/photo-1559651451-4203db8d2776?q=80&w=2012&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                    title: "The Ultimate 70+ Hours iOS Development Bootcamp",
                    author: "James Harrison",
                    rating: 5.0,
                    reviewCount: 34,
                    price: 2999
                )
            ]),
            .init(section: .udemyBusinessBanner(id: "6786"), body: [
                .udemyBusinessBanner(id: "rgrgrg", link: "https://business.udemy.com")
            ])
        ])
        collectionView.setDataSource(uiModel: uiModel)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    private func observe() {
        collectionView.eventPublisher.sink { [weak self] event in
            switch event {
            case .itemTapped(let item):
                self?.handleItemTapped(item: item)
            }
        }.store(in: &cancellabless)
    }
    private func handleItemTapped(item: HomeUIModel.Item) {
        switch item {
        case .mainBanner(_, _, _, _):
            break
        case .course(_, _, let title, _, _, _, _, _):
            showCourseDetailViewController(title: title)
        case .textHeader(_, _, _):
            break
        case .udemyBusinessBanner(_, let link):
            showSafariWebView(link: link)
        case .categoriesScroller(_, let titles):
            guard let title = titles.first else { return }
            print(title)
        case .featureCourse(_, _, _, _, _, _, _):
           break
        }
    }

    private func setupView() {
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func showCourseDetailViewController(title: String) {
        let viewController = CourseDetailViewController()
        viewController.setText(title: title)
        navigationController?.pushViewController(viewController, animated: true)
    }

    private func showSafariWebView(link: String) {
        guard let url = URL(string: link) else { return }
        let safariController = SFSafariViewController(url: url)
        navigationController?.present(safariController, animated: true)
    }

    private func loadJSON() {
        let response: APIResponse? = FileManager.modelFromJSON(fileName: "payload")
        print(response)
    }
}

