//
//  HomeCollectionView.swift
//  ios-udemy-home
//
//  Created by Planet iOS on 24/06/24.
//

import SwiftUI
import Combine

final class HomeCollectionView: UICollectionView {

    enum Event {
        case itemTapped(HomeUIModel.Item)
    }
    
    private var diffableDataSource: UICollectionViewDiffableDataSource<HomeUIModel.Section, HomeUIModel.Item>!
    private var uiModel: HomeUIModel?
    private let eventSubject = PassthroughSubject<Event, Never>()
    var eventPublisher: AnyPublisher<Event, Never> {
        return eventSubject.eraseToAnyPublisher()
    }

    init() {
        super.init(
            frame: .zero, collectionViewLayout: UICollectionViewFlowLayout()
        )
        collectionViewLayout = makeCompositionalLayout()
        registerCells()
        setupDataSource()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setDataSource(uiModel: HomeUIModel) {
        self.uiModel = uiModel
        self.applySnapshot()
    }

    private func registerCells() {
        register(MainBannerCollectionViewCell.self, forCellWithReuseIdentifier: MainBannerCollectionViewCell.nameIdentifier)
        register(TextHeaderCollectionViewCell.self, forCellWithReuseIdentifier: TextHeaderCollectionViewCell.nameIdentifier)
        register(CourseCollectionViewCell.self, forCellWithReuseIdentifier: CourseCollectionViewCell.nameIdentifier)
        register(CategoriesCollectionViewCell.self, forCellWithReuseIdentifier: CategoriesCollectionViewCell.nameIdentifier)
        register(FeatureCourseCollectionViewCell.self, forCellWithReuseIdentifier: FeatureCourseCollectionViewCell.nameIdentifier)
        register(UdemyBusinessCollectionViewCell.self, forCellWithReuseIdentifier: UdemyBusinessCollectionViewCell.nameIdentifier)
    }

    private func setupDataSource() {
        diffableDataSource = UICollectionViewDiffableDataSource(
            collectionView: self, 
            cellProvider: { collectionView, indexPath, item in
                switch item {
                case .mainBanner(
                    id: _,
                    imageLink: let imageLink,
                    title: let title,
                    caption: let caption
                ):
                    let cell = collectionView.dequeueReusableCell(
                        withReuseIdentifier: MainBannerCollectionViewCell.nameIdentifier,
                        for: indexPath
                    ) as! MainBannerCollectionViewCell
                    cell.configure(imageLink: imageLink, title: title, caption: caption)

                    return cell
                case .textHeader(id: _, text: let text, higlighterText: let highlightedText):
                    let cell = collectionView.dequeueReusableCell(
                        withReuseIdentifier: TextHeaderCollectionViewCell.nameIdentifier,
                        for: indexPath
                    ) as! TextHeaderCollectionViewCell
                    cell.configure(text: text, higlightedText: highlightedText)
                    cell.onTap = { [weak self] in
                        self?.eventSubject.send(.itemTapped(item))
                    }

                    return cell
                case .course(id: _, imageLink: let imageLink, title: let title, author: let author, rating: let rating, reviewCount: let reviewCount, price: let price, tag: let tag):
                    let cell = collectionView.dequeueReusableCell(
                        withReuseIdentifier: CourseCollectionViewCell.nameIdentifier,
                        for: indexPath
                    ) as! CourseCollectionViewCell
                    cell.configure(imageLink: imageLink, title: title, author: author, rating: rating, reviewCount: reviewCount, price: price, tag: tag)
                    cell.onTap = { [weak self] in
                        self?.eventSubject.send(.itemTapped(item))
                    }

                    return cell
                case .categoriesScroller(id: let id, titles: let titles):
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoriesCollectionViewCell.nameIdentifier, for: indexPath) as! CategoriesCollectionViewCell
                    cell.configure(titles: titles)
                    cell.onTap = { [weak self] title in
                        let selected = HomeUIModel.Item.categoriesScroller(id: id, titles: [title])
                        self?.eventSubject.send(.itemTapped(selected))
                    }

                    return cell
                case .featureCourse(id: _, imageLink: let imageLink, title: let title, author: let author, rating: let rating, reviewCount: let reviewCount, price: let price):
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeatureCourseCollectionViewCell.nameIdentifier, for: indexPath) as! FeatureCourseCollectionViewCell
                    cell.configure(imageLink: imageLink, title: title, author: author, rating: rating, reviewCount: reviewCount, price: price)
                    cell.onTap = { [weak self] in
                        self?.eventSubject.send(.itemTapped(item))
                    }

                    return cell
                case .udemyBusinessBanner(id: _, link: _):
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UdemyBusinessCollectionViewCell.nameIdentifier, for: indexPath) as! UdemyBusinessCollectionViewCell
                    cell.onTap = { [weak self] in
                        self?.eventSubject.send(.itemTapped(item))
                    }
                    return cell
                }
            })
    }

    private func applySnapshot() {
        var snapshot = NSDiffableDataSourceSnapshot<HomeUIModel.Section, HomeUIModel.Item>()
        uiModel?.sectionModels.forEach({ sectionModel in
            snapshot.appendSections([sectionModel.section])
            snapshot.appendItems(sectionModel.body, toSection: sectionModel.section)
        })
        diffableDataSource.apply(snapshot, animatingDifferences: false)
    }

    private func makeCompositionalLayout() -> UICollectionViewCompositionalLayout {

        let provider: UICollectionViewCompositionalLayoutSectionProvider = { [weak self] index, env in
            guard let self = self else { return nil }
            guard let sectionModel = self.uiModel?.sectionModels[index] else { return nil }
            switch sectionModel.section {
            case .mainBanner:
                return self.makeMainBannerSection()
            case .textHeader:
                guard case let .textHeader(_, text, _) = sectionModel.body.first else { return nil }
                return self.makeTextHeaderSection(text: text)
            case .coursesSwimlane:
                return self.makeCourseSwimlaneSection()
            case .categories:
                return self.makeCategoriesSection()
            case .featureCourse:
                return self.makeFeatureCourseSection()
            case .udemyBusinessBanner:
                return self.makeUdemyBusinessBannerSection()
            }
        }
        
        return UICollectionViewCompositionalLayout(sectionProvider: provider)
    }

    private func makeMainBannerSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let layoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(220))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: layoutSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 20, trailing: 0)
        return section
    }
    private func makeTextHeaderSection(text: String) -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let height = AttributedTappableLabel.heightForWidth(frame.size.width, text: text)
        let layoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(height))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: layoutSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 8, trailing: 20)
        return section
    }
    private func makeCourseSwimlaneSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let layoutSize = NSCollectionLayoutSize(widthDimension: .absolute(160), heightDimension: .absolute(200))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: layoutSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = 10
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 10, trailing: 20)
        section.orthogonalScrollingBehavior = .continuous
        return section
    }
    private func makeCategoriesSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let layoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(88))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: layoutSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = 10
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 20, trailing: 0)
        return section
    }
    private func makeFeatureCourseSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let layoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(230))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: layoutSize, subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 20, trailing: 20)
        return section
    }
    private func makeUdemyBusinessBannerSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let layoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(160))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: layoutSize, subitems: [item])
        group.interItemSpacing = NSCollectionLayoutSpacing.fixed(10)
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 20, trailing: 20)
        return section
    }
}
